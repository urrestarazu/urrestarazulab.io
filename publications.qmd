---
page-layout: article
engine: knitr
---

```{css}
#| echo: false
h4 {
  font-size: 25px;
  border-bottom: solid;
}
```

::: {.content-visible when-profile="english"}
#### Journal articles 
::: {.justify}

2024 [with A. García-Covelo, & A. Egurtzegi]. Sudurkaritasun fonologikoa Larrainen: /[h̃]{.ipa}/ bai, baina bokalik ez [Phonological nasality in Larraine Basque: there is /[h̃]{.ipa}/, but no nasal vowels]. *Fontes Linguae Vasconum* 137, 163-190.
[{{< iconify academicons open-access width="28" height="28" >}}](https://doi.org/10.35462/flv137.7)
[{{< iconify academicons doi width="23" height="23" >}}](https://doi.org/10.35462/flv137.7)

2024 [with A. Egurtzegi, D. Krajewska & C. Carignan]. An acoustic exploration of sibilant contrasts and sibilant merger in Mixean Basque. *Journal of the International Phonetic Association*
[{{< iconify academicons open-access width="28" height="28" >}}](https://doi.org/10.1017/S0025100324000045)
[{{< iconify academicons doi width="23" height="23" >}}](https://doi.org/10.1017/S0025100324000045)
[{{< iconify academicons osf width="23" height="23" >}}](https://osf.io/vctgw/)

2023 [with S. Coretta, J.V. Casillas, T. Roettger, et al.]. Multidimensional signals and analytic flexibility: Estimating degrees of freedom in human-speech analyses. *Advances in Methods and Practices in Psychological Science* 6(3).
[{{< iconify academicons open-access width="28" height="28" >}}](https://doi.org/10.1177/25152459231162567)
[{{< iconify academicons doi width="23" height="23" >}}](https://doi.org/10.1177/25152459231162567)
[{{< iconify academicons arxiv width="23" height="23" >}}](https://psyarxiv.com/q8t2k/)
[{{< iconify academicons osf width="23" height="23" >}}](https://osf.io/3bmcp/)
[{{< iconify mdi github width="25" height="25" >}}](https://github.com/many-speech-analyses/many_analyses). Code and discussion of our team's analysis can be accessed here [{{< iconify academicons osf width="23" height="23" >}}](https://osf.io/g9bq5/)

2023 Euskara Batu Zaharraren haustura: oinarri metodologikoak eta literaturaren berrikuspena [The split of Old Common Basque: methodological foundations and literature review]. *Anuario del Seminario de Filología Vasca "Julio de Urquijo"*. [{{< iconify academicons open-access width="28" height="28" >}}](https://doi.org/10.1387/asju.24118) [{{< iconify academicons doi width="23" height="23" >}}](https://doi.org/10.1387/asju.24118)

2019 [with B. Fernández, A. Berro & I. Orbegozo]. Mapping syntactic variation: the BiV database. *Linguistic Typology* 23(2). [{{< iconify academicons doi width="23" height="23" >}}](https://doi.org/10.1515/lingty-2019-0018)


#### Book chapters 

2022. Auzibidea Burundakoa da, eta euskara? XVII. mendeko euskara apurtxo batzuk eta Burundako euskara garaikidea alderatzen [Actually Burunda Basque? Comparing the Basque fragments from a 17th century trial proceedings and the current Burunda Basque]. In Dorota Krajewska, Sergio Monforte & Eneko Zuloaga (Eds.), *Aziti Bihia. Euskal filologia eta hizkuntzalaritzaren adarrak*. Leioa: UPV/EHU. pp. 203-227. [{{< iconify academicons open-access width="28" height="28" >}}](https://addi.ehu.es/bitstream/handle/10810/58047/UFGPD224578.pdf?sequence=1&isAllowed=y)

#### Proceedings 

2023 [with A. Egurtzegi & A. García-Covelo]. A nasalance-based study of the /[h]{.ipa}/ vs. /[h̃]{.ipa}/
opposition in Zuberoan Basque. *Proceedings of ICPhS 2023*. Prague: IPA. 3427-3431.
[{{< iconify academicons open-access width="28" height="28" >}}](https://guarant.cz/icphs2023/1047.pdf)


2018 [with I. Orbegozo, A. Berro & B. Fernández]. Basque in Variation (BiV): una base de datos online sobre la variación morfonsintáctica del euskera. In *Actas do XIII Congreso Internacional de Lingüística Xeral (CILX2018)*. Vigo: U. of Vigo. 677-684. [{{< iconify academicons open-access width="28" height="28" >}}](http://cilx2018.uvigo.gal/actas/pdf/646702.pdf)


#### Editions 

2023 [with O. Leturiaga & A. Lizardi] [guest editors]. *Hazitik liliak. Zenbait ekarpen Euskal Ikasketen ikerketan* [From the seeds to the flowers. Contributions to the Basque Studies]. Special issue of *Fontes Linguae Vasconum*, 135. [{{< iconify academicons open-access width="28" height="28" >}}](https://revistas.navarra.es/index.php/FLV/issue/view/133)

#### Other publications 

2021 [with A. Egurtzegi, G. Elordieta, O. Jauregi, D. Krajewska &amp; M.L. Oñederra]. *Nazioarteko Alfabeto Fonetikoa (2021ean moldatua)* [Basque translation of *The International Phonetic Alphabet*]. International Phonetic Association. [{{< iconify academicons open-access width="28" height="28" >}}](https://egurtzegi.github.io/papers/IPA_Kiel_2021_full_eus.pdf)
:::

::: {.justify style="font-size: 90%; padding-left: 15px; margin: 30px; border-top: solid; padding-top: 15px;"}
[{{< iconify academicons open-access width=21 height=21 >}}](https://sparcopen.org/open-access/) Link to the pdf of the publication (or to the volume where the paper can be accessed).
<br>
[{{< iconify academicons doi >}}](https://www.doi.org/) Link to the **Digital Object Identifier**. Usually, the website of the publication.
<br>
[{{< iconify academicons osf >}}](https://osf.io) Link to the **Open Science Foundation** of the research.
<br>
[{{< iconify academicons arxiv >}}](https://arxiv.org) Link to the **Arxiv** repository.
<br>
[{{< iconify teenyicons gitlab-solid >}}](https://about.gitlab.com) / [{{< iconify mdi github width=20 height=20 >}}](https://github.com) Link to the **Gitlab** or **Github** repositories.
:::
:::


::: {.content-visible when-profile="basque"}
#### Aldizkarietako argitalpenak
::: {.justify}

2024 [with A. García-Covelo, & A. Egurtzegi]. Sudurkaritasun fonologikoa Larrainen: /[h̃]{.ipa}/ bai, baina bokalik ez [Phonological nasality in Larraine Basque: there is /[h̃]{.ipa}/, but no nasal vowels]. *Fontes Linguae Vasconum* 137, 163-190.
[{{< iconify academicons open-access width="28" height="28" >}}](https://doi.org/10.35462/flv137.7)
[{{< iconify academicons doi width="23" height="23" >}}](https://doi.org/10.35462/flv137.7)

2024 [with A. Egurtzegi, D. Krajewska & C. Carignan]. An acoustic exploration of sibilant contrasts and sibilant merger in Mixean Basque. *Journal of the International Phonetic Association*
[{{< iconify academicons open-access width="28" height="28" >}}](https://doi.org/10.1017/S0025100324000045)
[{{< iconify academicons doi width="23" height="23" >}}](https://doi.org/10.1017/S0025100324000045)
[{{< iconify academicons osf width="23" height="23" >}}](https://osf.io/vctgw/)

2023 [with S. Coretta, J.V. Casillas, T. Roettger, et al.]. Multidimensional signals and analytic flexibility: Estimating degrees of freedom in human speech analyses. *Advances in Methods and Practices in Psychological Science* 6(3).
[{{< iconify academicons open-access width="28" height="28" >}}](https://doi.org/10.1177/25152459231162567)
[{{< iconify academicons doi width="23" height="23" >}}](https://doi.org/10.1177/25152459231162567)
[{{< iconify academicons arxiv width="23" height="23" >}}](https://psyarxiv.com/q8t2k/)
[{{< iconify academicons osf width="23" height="23" >}}](https://osf.io/3bmcp/)
[{{< iconify mdi github width="25" height="25" >}}](https://github.com/many-speech-analyses/many_analyses). Gure taldearen kodea eta eztabaida hemen [{{< iconify academicons osf width="23" height="23" >}}](https://osf.io/g9bq5/)

2023 Euskara Batu Zaharraren haustura: oinarri metodologikoak eta literaturaren berrikuspena. *ASJU*. [{{< iconify academicons open-access width="28" height="28" >}}](https://doi.org/10.1387/asju.24118) [{{< iconify academicons doi width="23" height="23" >}}](https://doi.org/10.1387/asju.24118)

2019 [with B. Fernández, A. Berro & I. Orbegozo]. Mapping syntactic variation: the BiV database. *Linguistic Typology* 23(2). [{{< iconify academicons doi width="23" height="23" >}}](https://doi.org/10.1515/lingty-2019-0018)


#### Liburuetako kapituluak


2022. Auzibidea Burundakoa da, eta euskara? XVII. mendeko euskara apurtxo batzuk eta Burundako euskara garaikidea alderatzen. In Dorota Krajewska, Sergio Monforte & Eneko Zuloaga (arg.), *Aziti Bihia. Euskal filologia eta hizkuntzalaritzaren adarrak*. Leioa: UPV/EHU. pp. 203-227. [{{< iconify academicons open-access width="28" height="28" >}}](https://addi.ehu.es/bitstream/handle/10810/58047/UFGPD224578.pdf?sequence=1&isAllowed=y)

#### Aktak 

2023 [with A. Egurtzegi & A. García-Covelo]. A nasalance-based study of the /[h]{.ipa}/ vs. /[h̃]{.ipa}/
opposition in Zuberoan Basque. *Proceedings of ICPhS 2023*, Praga: IPA. 3427-3431.
[{{< iconify academicons open-access width="28" height="28" >}}](https://guarant.cz/icphs2023/1047.pdf)


2018 [with I. Orbegozo, A. Berro & B. Fernández]. Basque in Variation (BiV): una base de datos online sobre la variación morfonsintáctica del euskera. In *Actas do XIII Congreso Internacional de Lingüística Xeral (CILX2018)*. Vigo: Vigoko unibertsitatea. 677-684. [{{< iconify academicons open-access width="28" height="28" >}}](http://cilx2018.uvigo.gal/actas/pdf/646702.pdf)


#### Edizioak

2023 [with O. Leturiaga & A. Lizardi] [guest editors]. *Hazitik liliak. Zenbait ekarpen Euskal Ikasketen ikerketan*. *Fontes Linguae Vasconum* aldizkariko alde berezia, 135. [{{< iconify academicons open-access width="28" height="28" >}}](https://revistas.navarra.es/index.php/FLV/issue/view/133)

#### Bestelako argitalpenak

2021 [with A. Egurtzegi, G. Elordieta, O. Jauregi, D. Krajewska &amp; M.L. Oñederra]. *Nazioarteko Alfabeto Fonetikoa (2021ean moldatua)*. [{{< iconify academicons open-access width="28" height="28" >}}](https://egurtzegi.github.io/papers/IPA_Kiel_2021_full_eus.pdf)
:::

::: {.justify style="font-size: 90%; padding-left: 15px; margin: 30px; border-top: solid; padding-top: 15px;"}
[{{< iconify academicons open-access width=21 height=21 >}}](https://sparcopen.org/open-access/) Artitalpenaren pdf-ra daraman esteka (edo lana duen alera daramana).
<br>
[{{< iconify academicons doi >}}](https://www.doi.org/) Argitalpenaren **Digital Object Identifier**era daraman esteka. Oro har, argitalpenaren webgunea izango da.
<br>
[{{< iconify academicons osf >}}](https://osf.io) Ikerketaren **Open Science Foundation** biltegira daraman esteka.
<br>
[{{< iconify academicons arxiv >}}](https://arxiv.org) **Arxiv** biltegira daraman esteka.
<br>
[{{< iconify teenyicons gitlab-solid >}}](https://about.gitlab.com) / [{{< iconify mdi github width=20 height=20 >}}](https://github.com) **Gitlab** edo **Github** biltegietara daraman esteka.
:::
:::
