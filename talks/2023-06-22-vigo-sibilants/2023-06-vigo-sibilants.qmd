---
title: "An acoustic exploration of sibilant contrasts and sibilant merger in Mixean Basque"
Host: "International Conference on Experimental Phonetics 2023"
abstract: Basque once had six voiceless sibilants common to all dialects, the lamino-alveolar, apico-alveolar, and postalveolar fricatives (/[s̻, s̺, ʃ]{.ipa}/), and their affricate counterparts, /[t͡s̻, t͡s̺, t͡ʃ]{.ipa}/ (Egurtzegi, 2013; Hualde, 2003; Mitxelena, 1977/2011). Mixean High Navarrese is an endangered variety of Basque (Camino, 2016) with a more complex sibilants system (/[s̻, s̺, ʃ, t͡s̻, t͡s̺, t͡ʃ, z̻, z̺, ʒ, d͡z̺]{.ipa}/). Nevertheless, a merger collapsing the three apico-alveolar sibilants with the three correspondent postalveolars was (preliminarily) proposed in the literature (Egurtzegi & Carignan, 2020).  This exploratory study investigates sibilants in Mixean to (a) provide a description of their acoustics, and (b) elucidate how many categories can be proposed based only on acoustical data. To that end, we analyze metrics reflecting the place of articulation (spectral moments, and especially the center of gravity, CoG), including also the temporal dynamics of CoG (using Discrete Cosine Transformation of CoG measured in 9 intervals of the phone). We also examine the changes in CoG provoked by the environment in which the phone is produced. The study is based on free-conversation data recorded between 2005 and 2015 in various villages of the Mixe region by Iñaki Camino (see Camino 2016), which comprise 10 subjects (three female, seven male) aged between 80 and 85 years. In total, the participants produced 1912 sibilants. The results show that, as regards spectral moments, CoG distinguishes lamino-alveolar sibilants from apico-alveolar and postalveolar sibilants. The remaining spectral moments fail to show significant differences between apico-alveolar and postalveolar series. This is in line with Egurtzegi and Carignan (2020) study of the same data set, where, on the basis of CoG values, a possible merger between apico-alveolar and postalveolar sibilants in this variety was proposed. However, there appears to be a difference between apico-alveolar and postalveolar sibilants in the temporal dynamics of frequency values. For example, for fricatives the interval with the highest CoG was 55-65% of the phone for both alveolar voiceless fricatives, but 45-55% for the voiceless postalveolar one. These differences were confirmed with Discrete Cosine Transformation analysis. As for the influence of the phonetic environment on CoG values, the contrasts or lack of them are maintained regardless of the context. In particular, the apico-alveolar and postalveolar phones are not significantly different in any environment. However, we have found several potentially interesting coarticulation effects. For example, we observe that, among the voiceless fricatives, the contrast between /[s̻]{.ipa}/ and /[s̺]{.ipa}/ is best marked in word-initial and prevocalic position, where the differences between CoG values are greatest. Thus, this paper offers a thorough exploration of the sibilants in one particular variety of Basque, but also aims to show an example of how to approach the acoustic description of a variety with an undetermined number of sibilant contrasts.
author: 
  - name: Ander Egurtzegi
    affiliation: [CNRS - IKER UMR 5478]
  - name: Dorota Krajewska
    affiliation: UPV/EHU
  - name: Christopher Carignan
    affiliation: Speech Hearing and Phonetic Sciences, University College London
  - name: Iñigo Urrestarazu-Porta
    affiliation: [CNRS - IKER UMR 5478, UPV/EHU, UPPA]
date: 06-22-2023
url: https://urrestarazu.gitlab.io/slides/2023-vigo-sibilants.html
image: cover.png
categories: [Amikuze, sibilants, Phonetics, Basque, Language documentation]
engine: knitr
---

::: {.content-visible when-profile="english"}
Click [here](https://urrestarazu.gitlab.io/slides/2023-vigo-sibilants.html) for the link to the slide.

<iframe src="https://urrestarazu.gitlab.io/slides/2023-vigo-sibilants.html" width="100%" height="450px"></iframe>
:::

::: {.content-visible when-profile="basque"}
Klik egin [hemen](`r rmarkdown::metadata$url`) filminetara joateko.

<iframe src="`r rmarkdown::metadata$url`" width="100%" height="450px"></iframe>
:::
