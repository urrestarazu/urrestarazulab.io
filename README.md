# Todo list:

- [x] Adjust theming
- [x] Implement a Basque language version
- [ ] Create a cool cv in the cv tab and link the pdf there
- [x] Talks tab
- [x] Add slide repositories to link them in the talks tab
- [x] Colour the ipa transcription of the name
- [x] Adjust the padding from the About me to the ipa transcription
- [x] Publications tab width
- [x] Change title font-size


### Adjust theming

Quarto darkly and cosmo themes don't share the same navbar specs. In darkly the bar is bigger, and changing from one theme to the other results in a funny movement. I should try to find the css specs of each theme and make thouse match.

Solved by copying darkly theme scss and changing fonts colours.


### Implement Basque

I learnt it from a [blog post](https://quarto-dev.marioangst.com/en/blog/posts/multi-language-quarto/) by [Mario Angst](https://quarto-dev.marioangst.com/en/), but:

- The name of the yml profile must be separated by `-`, not by `.`. So, they're `_quarto-english.yml`, `_quarto-basque.yml`.
- The command for language imput must be `{.content-visible when-profile=""}`. Otherwise it didn't work very well.
- I didn't follow the organizing scheme outlined by Mario, i.e. in the output, one folder for every language. Instead, I created the english as before (straight into public), and the Basque version in it. Just in case the other way didn't work fine when publishing.
- The pointed the language references to the created html files. The linking solution by Mario returned a 404 error.
- For some reason, I have to render them separately AND! English first. Otherwise, it replaces the Basque version. 


### Cool cv

Check https://emitanaka.org.


### Colour ipa name

Different grays in each theme scss; and a little padding

### Talks tab

Create talks tab with links to the actual slides.
I like very much how Emi Tanaka has implemented it: https://emitanaka.org).

I managed to do it for one presentation. I will link amikuze-sibilants-vigo & larraine-nasals-icphs next

### Slides repositories


### Publications tab width

I think it's to wide. A thinner layout looks better (see egurtzegi.github.io). I tried through a css spec in that qmd, and it worked... until you returned to the main page; it changed to the narrow setting.

Solution: set page-layout in every qmd. full for the index (i.e. home page), article for the publications

### Title font size
It's too big, and doesn't fit properly. Need to reduce its size. Had to change the h1 css (just for that document)
